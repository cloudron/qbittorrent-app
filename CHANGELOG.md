[0.1.0]
* Initial version for VueTorrent 1.8.0

[0.2.0]
* Various manifest fixes

[1.0.0]
* Initial stable version
* Replace with proxyAuth

[1.1.0]
* VueTorrent updated to 2.0.1

[1.2.0]
* Update VueTorrent to 2.1.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.1.0)
* filters: Allow to disable filters temporarily (6f9ee5d)
* localization: Bringing back Russian locale (#1281) (20dc684)
* ShareLimit: Add torrent share limit dialog (ed0991e)
* title: Ability to set custom browser tab title (1f58005)

[1.2.1]
* Update VueTorrent to 2.1.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.1.1)
* Navbar: Add missing bind on right drawer setting (#1294) (672ab08)
* RightClickMenu: Fix target not being selected on long press (#1295) (d80cc35)
* settings: Update drag handling to completed properties (#1286) (dd53e6f)

[1.3.0]
* Update VueTorrent to 2.2.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.2.0)
* AddTorrentDialog: Rework dialog to add missing parameters (#1323) (f69851c)
* ConnectionStatusDialog: Add DHT node and active peer connections (#1312) (6f01c00)
* Content: Add menu to set file priority (#1333) (f83fe06)
* Overview: Add keyboard shortcuts (#1335) (15a20a3)
* preferences: Update settings page to include missing preferences (#1296) (e034071)
* RSS: Add conf to use ID instead of link (#1334) (6af7537)

[2.0.0]
* **Important Breaking Change**:
    * This app has switched to use the built-in qBittorrent authentication
    * The default credentials is username `admin` and password `adminadmin`
    * This reason of this change is because the VueTorrent UI does not work with authentication. Additionally, there are many tools that integrate with the qBittorrent API. These tools break when Cloudron's proxyAuth is in use.

[2.0.1]
* Update VueTorrent to 2.3.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.3.0)
* history: Add history to some fields (#1361) (ef50c6c)
* Settings: Handle tab routes in settings (#1355) (fdad814)
* Advanced: Network interfaces doesn't fill properly (#1337) (3c44709)
* tabs: Remove swipe gestures (#1356) (41e021c)
* Add download path management (#1359) (988ecaa)
* add translations cron (#1367) (02d97f5)
* localization: Add missing keys to Tolgee (#1349) (3adeea4)

[2.0.2]
* Update VueTorrent to 2.4.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.4.0)
* Chips: Add option to hide values if empty (#1380) (6b94f6e)
* dashboard: Add display mode configuration (#1184) (cd695ab)
* AddTorrentDialog: Fix falsy values not sent to qbit (#1377) (8abda97)
* duration: Values greater than a month weren't displayed (#1381) (8ba6d12)
* helpers: Rework toPrecision (#1394) (69398e0)
* DnDZone: Open add dialog on drop (#1374) (5635099)
* logs: Prevent duplication and add message filter (#1398) (a1c900c)
* Prevent rounding for relative time values (#1378) (86bba4e)
* Update translations (#1373) (a9d9721)

[2.1.0]
* Update VueTorrent to 2.5.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.5.0)
* add-torrent: Use scrollable dialog instead of manual infinite scroll (#1442) (7288861)
* center login screen (f773756)
* dev: Add MockProvider (#1435) (f5524b7)
* SpeedGraph: Init values with null (#1422) (51d58cf)
* tweak torrent title (0e67cea)

[2.1.1]
* Update VueTorrent to 2.6.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.6.0)
* add check latest version #941 (6eb2c15)
* bring back active filter #1280 (afe4fa5)
* bring back settings import #1322 (53fe2e5)
* darkmode toggle #1478 (97def1d)
* pagination: Allow for custom values for pagination size (#1482) (8db1a29)
* PWA: Use Network-only strategy for service worker (#1468) (f4842c4)
* search box spacing (#1458) (4ec69b6)
* UI text (#1481) (6b03b31)
* use search bar #891 (67d76b6)

[2.2.0]
* Update VueTorrent to 2.7.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.7.0)
* rss: Update rule form to include enabled and torrentParams (#1501) (aefa996)
* import: Update field validation to prevent false negative (#1495) (35d8700)
* localization: Add missing ru pluralization rule (#1500) (01ac1bb)
* progress: Remove striping on table view (#1490) (3256b1b)
* RSS: Fetch feeds when opening the RSS Rule dialog (#1496) (7421e28)
* autofill: Add valid id on login fields (#1507) (fc76202)
* SpeedGraph: Add time as X axis instead of computed step (#1494) (d750370)

[2.2.1]
* Update VueTorrent to 2.7.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.7.1)
* auto-reload in unsecure contexts (#1532) (e8c5e04)
* auto-reload when disabling VueTorrent (#1531) (aaf2ecb)
* content: Add right click workaround for apple devices (#1521) (94949ec)
* content: file list not loading (#1524) (8a9bf16)
* Peers: Wrap IPv6 to display ban button (#1517) (dfd7bf7)
* pluralization logic in i18n.ts for "ru" (#1537) (bd60cb7)
* preferences: Update banned IP field when banning from the Peers tab (#1518) (55f4865)
* QbitProvider: manually add indexes to getTorrentFiles response to provide compatibility with older version (< 4.4.0) (#1510) (512d177)
* settings: Infinite scroll value not recognized (#1520) (f540ccd)
* AddTorrentDialog: Deduplicate AddTorrent form and AddTorrentParamsForm (#1541) (8e14e7c)
* Content tab: Allow filtering files by name (#1542) (47de865)
* Info: add torrent properties (#1528) (196f5e3)
* sw: Use relative addresses for better compatibility with reverse proxies (#1511) (a3dc22e)
* TorrentDetail: Reduce API calls by centralizing files data (#1529) (e92d305)

[2.2.2]
* Update VueTorrent to 2.7.2
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.7.2)
* Add category / tag chip color (#1539) (05b3e35)
* Add ratio colors (#1581) (4c6410d)
* connection status: Improve external IP error message (#1587) (d84f00e)
* content: Add clear icon on search input (#1583) (5a989aa)
* DnDZone: Prevent stuck zone when leaving too early (#1568) (2943ed1)

[2.2.3]
* Update VueTorrent to 2.7.3
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.7.3)
* AddTorrentDialog: Unset download path if default is unchecked (#1619) (b6db317)
* announced IP not parsed correctly (#1601) (c76caef)
* RSS Rules: Download limits not sent correctly to qbit (#1622) (a8dbea8)
* RSS Rules: Fix last match not displaying (#1618) (a4ca1c9)
* RSS Rules: Include deprecated fields for better retro-compatibility (#1621) (cca5a36)
* ServiceWorker: sw.js url (#1609) (702ff80)
* TorrentDetail: Differentiate fullName for the root node(#1616) (#1617) (4ce5eca)
* add title navigation (#1590) (13afd79)
* chip colors: Add feature toggle (#1600) (d311a69)
* fetch Geolocation & ISP details (#1602) (902b044)
* Replace ratio_time_limit by seeding_time_limit (#1611) (6edde21)
* special values for ratio limit (#1613) (2e379cf)
* torrentCard: Add select all / none buttons (#1604) (492c0dd)
* TorrentCard: decrease chip size (#1608) (08a49f0)
* TorrentCard: decrease chip size for table view (#1610) (3413ef0)

[2.3.0]
* Update VueTorrent to 2.8.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.8.0)
* Add optional backend handling (#1547) (d513e09)
* TorrentDetail: add bulk renaming (#1624) (a7ebcb5)
* Add missing forcedMetaDL state (#1652) (1013151)
* AddTorrentParams: Convert seeding time to minutes manually (#1649) (3d05859)
* MagnetHandler: Don't show add dialog if not authenticated (#1657) (a0cdffd)
* seeds and peers shows same value (#1632) (bd09a8a)
* share limits: Revert and fix display (#1654) (2eca39e)
* WebUI: Bypass auth subnet whitelist not working (#1645) (4cbb456)

[2.3.1]
* Update VueTorrent to 2.8.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.8.1)
* MagnetHandler: Add alias to old link for better compatibility (#1682) (3b1f5a3)
* Peers: Show client, `peer_id`, and i18n strings (#1579) (cbfe2c9)
* Reword torrent priority to queue position (#1667) (719a29d)
* RSS: add refresh all rss feeds button to rss articles (#1679) (4ddf5f3)
* settings: Prettify exported JSON (#1672) (84b9c12)
* TorrentDetail: Use colors for ratio and chips (#1662) (c902d62)

[2.4.0]
* Update VueTorrent to 2.9.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.9.0)
* Add theme variants (#1641) (6a20007)
* pages: Deploy mocked demo version (#1690) (f8695bf)
* tags: added remove all button for tags in rightClick menu (#1701) (2a4aedc)
* tags: option to have tags/categories without the “pill” shape (#1492) (#1708) (c3fc963)

[2.5.0]
* Update VueTorrent to 2.10.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.10.1)

[2.5.1]
* Update VueTorrent to 2.11.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.11.0)

[2.5.2]
* Update VueTorrent to 2.11.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.11.1)

[2.5.3]
* Update VueTorrent to 2.11.2
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.11.2)

[2.6.0]
* Update VueTorrent to 2.12.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.12.0)
* Add global error handler (#1834) (b241617)
* filters: Add conjunctive / disjunctive filter type (#1855) (31378e0)

[2.6.1]
* Update VueTorrent to 2.13.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.13.0)
* backend: Auto-configure on load (#1904) (909d26a)
* responsive search bar placement (#1883) (8b9a3a3)
* settings: Add pagination position configuration (#1870) (b82c124)
* tooltip on torrent state indicator for table view (#1890) (c6722b1)
* Increase menu overflow threshold (#1887) (d02a29b)
* RSS: "mark as read" on feed not working (#1897) (c290202)
* Disallow robots indexing of the WebUI to reduce exposure (#1903) (b829924)
* Login: Improve error message (#1901) (7c3fb61)
* router: Add catch-all route (#1886) (e7abdb2)

[2.6.2]
* Update VueTorrent to 2.13.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.13.1)
* backend: Update toast condition when backend unreachable (#1911) (79e3dce)
* Light theme: Use theme color for TorrentSearchbar (#1905) (e117f72)
* RSS: Display all feeds even if empty (#1913) (d8969e0)

[2.6.3]
* Update VueTorrent to 2.13.3
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.13.3)

[2.6.3-1]
* Update VueTorrent to 2.13.3
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.13.3)

[2.6.4]
* Update VueTorrent to 2.14.1
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.14.1)
* Dashboard: Update sort algorithm for queue position (#​1956) (80d4975)
* export: Prevent errors while generating zip (#​1961) (4ce4beb)
* Peers: Fix shifted columns when flag is missing (#​1958) (1e7b288)
* DeleteTorrentDialog: Remap incorrect binding and persist on-demand (#​1953) (df5e0a4)
* SearchEngine: Improve download feedback (#​1954) (c237425)
* torrentState: Add toggle for emojis (#​1962) (7e24e13)

[2.7.1]
* Update VueTorrent to 2.15.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.15.0)
* torrentCreator: Add new torrent creator view (#​1973) (6e6cc48)
* RightClick: Overhaul of menu item generation for better performance (#​1976) (d1301b1)

[2.8.0]
* Update VueTorrent to 2.16.0
* [Full changes](https://github.com/WDaan/VueTorrent/releases/tag/v2.16.0)
* settings: Only fetch external IP data if enabled (#2001) (73076d8)
* ActiveFilters: Force-enable filter after clear (#1998) (9faddbd)
* AddTorrents: Don't override savePath on empty category savePath (#1999) (119ab36)
* Logs: Persist filters in localStorage (#2000) (37317f8)
* RightClick: Handle issue where certain bottom values would not render (#1985) (2878286)
* SearchEngine: Fix broken category list (#2006) (ca23c5a)
* SearchEngine: Sort plugins by name (#2007) (54f5289)

[2.9.0]
* Update VueTorrent to 2.17.0
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.17.0)
* Add remove unused tags/categories actions ([#&#8203;2026](https://github.com/VueTorrent/VueTorrent/issues/2026)) ([f962912](https://github.com/VueTorrent/VueTorrent/commit/f9629125a720c8ca225c1865d8687db4f06bf7f6))
* **Content:** Handle keyboard inputs ([#&#8203;2014](https://github.com/VueTorrent/VueTorrent/issues/2014)) ([8253831](https://github.com/VueTorrent/VueTorrent/commit/8253831de3f9a7b2dfe7421f64ffbab8b8ade9ce))
* **filters:** Add torrent count for each filter values ([#&#8203;2024](https://github.com/VueTorrent/VueTorrent/issues/2024)) ([a44b15d](https://github.com/VueTorrent/VueTorrent/commit/a44b15ddf1069377dce22545772ce973aca8ad59))
* **Dashboard:** Make checkbox click events even with torrent card ([#&#8203;2011](https://github.com/VueTorrent/VueTorrent/issues/2011)) ([a3aa263](https://github.com/VueTorrent/VueTorrent/commit/a3aa26362d07e7c2635bfc136a5444840a8450ab))
* **filters:** Remove seeding state from inactive filter ([#&#8203;2015](https://github.com/VueTorrent/VueTorrent/issues/2015)) ([56d6124](https://github.com/VueTorrent/VueTorrent/commit/56d6124658c39516c143020c22ce2d4037fa0e67))
* **selection:** Filter out torrents not matching filters ([#&#8203;2033](https://github.com/VueTorrent/VueTorrent/issues/2033)) ([0b0be6f](https://github.com/VueTorrent/VueTorrent/commit/0b0be6fe05b0fbe668d8adea2dc1c9c006afb1b2))
* **Settings/Advanced:** Add missing disk IO type ([#&#8203;2020](https://github.com/VueTorrent/VueTorrent/issues/2020)) ([7c3c9ea](https://github.com/VueTorrent/VueTorrent/commit/7c3c9ea0ec665784d1e75be3b05316892c0c1fb9))
* **trackers:** Update "untracked" filter matching ([#&#8203;2032](https://github.com/VueTorrent/VueTorrent/issues/2032)) ([89fe1ff](https://github.com/VueTorrent/VueTorrent/commit/89fe1ff95358b7e0be3de823be2f279dab45e130))
* Adding secondary sorts for peers and seeds properties ([#&#8203;2021](https://github.com/VueTorrent/VueTorrent/issues/2021)) ([4827c28](https://github.com/VueTorrent/VueTorrent/commit/4827c28de0f848be0448f45c0339cb09d0ea47ad))
* **filters:** Add "stalled" states to offline preset ([#&#8203;2013](https://github.com/VueTorrent/VueTorrent/issues/2013)) ([d59f92a](https://github.com/VueTorrent/VueTorrent/commit/d59f92a6c77198855d79b4affdce2b4ed2987f71))
* **RSS:** Add "new" chip for increased contrast ([#&#8203;2031](https://github.com/VueTorrent/VueTorrent/issues/2031)) ([71a7c1f](https://github.com/VueTorrent/VueTorrent/commit/71a7c1f9e931f25e42ce469e87a2b1721b90bf3d))

[2.10.0]
* Update VueTorrent to 2.18.0
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.18.0)
* **Logs:** Add sort support ([#&#8203;2044](https://github.com/VueTorrent/VueTorrent/issues/2044)) ([781c802](https://github.com/VueTorrent/VueTorrent/commit/781c80267e62e513f638f1c25a20363573877b76))
* **Navbar:** Make speed cards clickable to select dl & ul filters ([#&#8203;2043](https://github.com/VueTorrent/VueTorrent/issues/2043)) ([5a0ad41](https://github.com/VueTorrent/VueTorrent/commit/5a0ad417dc8b6d4dc64c15eb2ba50f8b5e4ffafc))
* **filters:** Prevent re-enable on app launch ([#&#8203;2039](https://github.com/VueTorrent/VueTorrent/issues/2039)) ([82b9a91](https://github.com/VueTorrent/VueTorrent/commit/82b9a91b6374104e6316a58d04eb1bec6385d979))
* **Overview:** Update outdated locale keys ([#&#8203;2045](https://github.com/VueTorrent/VueTorrent/issues/2045)) ([4c4ff46](https://github.com/VueTorrent/VueTorrent/commit/4c4ff460313404b7550fb81690f6fff7f27e3b3a))
* **TableView:** Set max width of torrent name to 40% ([#&#8203;2042](https://github.com/VueTorrent/VueTorrent/issues/2042)) ([401d48d](https://github.com/VueTorrent/VueTorrent/commit/401d48d4bbe633ff7e2a165e0c83259f20e45bb9))
* **filters:** Differentiate "Untracked" and "Not working" trackers ([#&#8203;2038](https://github.com/VueTorrent/VueTorrent/issues/2038)) ([65fcd53](https://github.com/VueTorrent/VueTorrent/commit/65fcd534c06d9a840bf1193e06cf38b2ccb27463))

[2.11.0]
* Update VueTorrent to 2.19.0
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.19.0)
* **Content:** Add expand / collapse all actions to right-click menu ([#&#8203;2081](https://github.com/VueTorrent/VueTorrent/issues/2081)) ([3ab44a5](https://github.com/VueTorrent/VueTorrent/commit/3ab44a5dfeb8eb909a068be018e61ccb4e3ccd0b))
* **RightClick:** Add bulk update tracker dialog ([#&#8203;2057](https://github.com/VueTorrent/VueTorrent/issues/2057)) ([7a57d22](https://github.com/VueTorrent/VueTorrent/commit/7a57d226389e143364e348532b798dcbe46ad0f4))
* **filters:** Don't count offline torrent in "Not working" tracker filter ([#&#8203;2051](https://github.com/VueTorrent/VueTorrent/issues/2051)) ([d59cae7](https://github.com/VueTorrent/VueTorrent/commit/d59cae7ccff87abbc7b62dbb05013d04ee891895))
* **RSS:** Prevent wrap on "NEW" chip ([#&#8203;2054](https://github.com/VueTorrent/VueTorrent/issues/2054)) ([eb82416](https://github.com/VueTorrent/VueTorrent/commit/eb82416a31341c36f3dd0c6802db001ed84580e4))
* **Settings:** Fix unusable i2p settings ([#&#8203;2076](https://github.com/VueTorrent/VueTorrent/issues/2076)) ([9a2efcf](https://github.com/VueTorrent/VueTorrent/commit/9a2efcf8f0d5ec37c7f6beb271ede4b5549a7e65))
* **TorrentCreator:** Add source field for qbit 5.0.3+ ([#&#8203;2069](https://github.com/VueTorrent/VueTorrent/issues/2069)) ([9f73baa](https://github.com/VueTorrent/VueTorrent/commit/9f73baaa30674e7740280545a5409ffa132f125e))
* **backend:** Rework sync behaviour ([#&#8203;2050](https://github.com/VueTorrent/VueTorrent/issues/2050)) ([542e5c7](https://github.com/VueTorrent/VueTorrent/commit/542e5c7d002f5ad8e58ca53ebd8e8de830213acc))
* **Content:** Allow keyboard to collapse parent folder with left arrow ([#&#8203;2080](https://github.com/VueTorrent/VueTorrent/issues/2080)) ([3f18e20](https://github.com/VueTorrent/VueTorrent/commit/3f18e20a89c95ce1c0453e42dc1179acc5e00a8c))
* **SearchEngine:** Update mobile variant to be more compact ([#&#8203;2059](https://github.com/VueTorrent/VueTorrent/issues/2059)) ([2816e4f](https://github.com/VueTorrent/VueTorrent/commit/2816e4fb311c71818bb0839d4624f0b7b4999c17))
* **TorrentDetail:** Remember last opened tab ([#&#8203;2082](https://github.com/VueTorrent/VueTorrent/issues/2082)) ([5088d26](https://github.com/VueTorrent/VueTorrent/commit/5088d262c2fc996f9a59e459f6b213a6443cc5c8))
* **trackers:** Improve overall UX ([#&#8203;2071](https://github.com/VueTorrent/VueTorrent/issues/2071)) ([41afc40](https://github.com/VueTorrent/VueTorrent/commit/41afc40ba216a80be604afc1aae976b41a3850d3))

[2.12.0]
* Update VueTorrent to 2.20.1
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.20.1)
* **content:** Revert vue-concurrency breaking reactivity ([a5ca21c](https://github.com/VueTorrent/VueTorrent/commit/a5ca21c376b77dbc379e56bafa443d28ec0d5038))
* **TorrentDetail:** Add torrent navigation buttons ([#&#8203;2108](https://github.com/VueTorrent/VueTorrent/issues/2108)) ([488958d](https://github.com/VueTorrent/VueTorrent/commit/488958db10245ec5eac3edd3f170690667254972))
* **Content:** Tree not expanding on load ([#&#8203;2095](https://github.com/VueTorrent/VueTorrent/issues/2095)) ([8792e3d](https://github.com/VueTorrent/VueTorrent/commit/8792e3d1d85c429eafcf0ce882ea1e8bb28f99b8))
* **Logs:** Prevent duplication on page refresh ([#&#8203;2100](https://github.com/VueTorrent/VueTorrent/issues/2100)) ([4368af9](https://github.com/VueTorrent/VueTorrent/commit/4368af9e0122eec4f4e6da51fd60df61b4b8f8f1))
* **paste:** Prevent blocking paste event on some text boxes ([#&#8203;2112](https://github.com/VueTorrent/VueTorrent/issues/2112)) ([c761591](https://github.com/VueTorrent/VueTorrent/commit/c76159188bc777e670700f25332b3a8ef3622520))
* **RSS:** Display feeds with empty articles ([#&#8203;2096](https://github.com/VueTorrent/VueTorrent/issues/2096)) ([a6730ab](https://github.com/VueTorrent/VueTorrent/commit/a6730ab440c1a547d38db1a823ef31291616150d))

[2.13.0]
* Update VueTorrent to 2.21.0
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.21.0)
* Add OLED theme ([#&#8203;2125](https://github.com/VueTorrent/VueTorrent/issues/2125)) ([02028c2](https://github.com/VueTorrent/VueTorrent/commit/02028c2954415cc9d050cb129482ff04463975ac))
* **Dashboard:** Remove empty space when pagination is hidden ([#&#8203;2120](https://github.com/VueTorrent/VueTorrent/issues/2120)) ([8dfdd1a](https://github.com/VueTorrent/VueTorrent/commit/8dfdd1a9cebdc4df3cd8f1dd6d40fd9532e1dde8))
* **backend:** Reduce network sync requests ([#&#8203;2122](https://github.com/VueTorrent/VueTorrent/issues/2122)) ([e2492aa](https://github.com/VueTorrent/VueTorrent/commit/e2492aacbbcb2723828fcfb87b17d49afe6214c3))
* **backend:** Sync display mode and sort ([#&#8203;2116](https://github.com/VueTorrent/VueTorrent/issues/2116)) ([e0fc708](https://github.com/VueTorrent/VueTorrent/commit/e0fc70809d14c1f4ade69c5ab27b0e865e9b1c1c))

[2.14.0]
* Update VueTorrent to 2.22.0
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.22.0)
* **Dashboard:** Add missing fields as dashboard items ([#&#8203;2144](https://github.com/VueTorrent/VueTorrent/issues/2144)) ([a492884](https://github.com/VueTorrent/VueTorrent/commit/a4928841fe153a4ee9e31e03bc990ccf4365e198))
* **backend:** Sort option isn't synced with table header ([#&#8203;2128](https://github.com/VueTorrent/VueTorrent/issues/2128)) ([1e00212](https://github.com/VueTorrent/VueTorrent/commit/1e0021213b671337d273ba968607917872f32064))
* **Overview:** Use correct selected size ([#&#8203;2142](https://github.com/VueTorrent/VueTorrent/issues/2142)) ([84bfef0](https://github.com/VueTorrent/VueTorrent/commit/84bfef0808d89bf6880c87a3c6822917ba96c5d3))
* **polyfills:** Add "toSorted" polyfill to support older browsers ([#&#8203;2137](https://github.com/VueTorrent/VueTorrent/issues/2137)) ([d92197c](https://github.com/VueTorrent/VueTorrent/commit/d92197cbc2e5e5da38a412ce6e57323cd51fc305))
* **Settings/Advanced:** Update unit conversions ([#&#8203;2140](https://github.com/VueTorrent/VueTorrent/issues/2140)) ([db85dd5](https://github.com/VueTorrent/VueTorrent/commit/db85dd5d2ff195c0c0578fa8df9725e41e3f5d1b))
* **Settings:** Update "delete torrent files afterwards" not working ([#&#8203;2135](https://github.com/VueTorrent/VueTorrent/issues/2135)) ([3dbfd06](https://github.com/VueTorrent/VueTorrent/commit/3dbfd067f9e2855c0f66ee78f12a794014a1a788))
* **Content:** Spacebar takes selection into account ([#&#8203;2141](https://github.com/VueTorrent/VueTorrent/issues/2141)) ([3bc6f06](https://github.com/VueTorrent/VueTorrent/commit/3bc6f06fae6430f1700f8de29eb767239885b02e))

[2.15.0]
* Update VueTorrent to 2.23.0
* [Full Changelog](https://github.com/WDaan/VueTorrent/releases/tag/v2.23.0)
* **Settings:** Add option to collapse content items by default ([#&#8203;2180](https://github.com/VueTorrent/VueTorrent/issues/2180)) ([f9da988](https://github.com/VueTorrent/VueTorrent/commit/f9da98880cbf67dfdbe4361ae2822be73c7246f6))
* **AddTorrentDialog:** Remove default value for speed limits ([#&#8203;2172](https://github.com/VueTorrent/VueTorrent/issues/2172)) ([3fc700c](https://github.com/VueTorrent/VueTorrent/commit/3fc700c57527a0f62ecdeef99d568bb66d059c91))
* **backend:** Torrent properties not synced more than once ([#&#8203;2163](https://github.com/VueTorrent/VueTorrent/issues/2163)) ([90c2a69](https://github.com/VueTorrent/VueTorrent/commit/90c2a693c66db35c6449aafafe8a23fc2f9cc39b))
* **Settings:** Add warning message when using high pagination values ([#&#8203;2176](https://github.com/VueTorrent/VueTorrent/issues/2176)) ([cbeb74e](https://github.com/VueTorrent/VueTorrent/commit/cbeb74eb0e7b385c8ddcfde04399dae3d1d390a4))

[2.16.0]
* Base image 5.0.0
* **note**: the new qBittorrent generates a random password (in the logs) if you did not change the admin password explicitly with the previous version

