#!/bin/bash
set -eu

if [[ ! -d /app/data/qBittorrent ]]; then
    echo "==> Initial config"

    mkdir -p /app/data/qBittorrent/config

    cat /app/pkg/qBittorrent.conf > /app/data/qBittorrent/config/qBittorrent.conf

    cat >> /app/data/qBittorrent/config/qBittorrent.conf << EOF
MailNotification\req_auth=true
MailNotification\req_ssl=false
MailNotification\sender=${CLOUDRON_MAIL_FROM}
MailNotification\smtp_server=${CLOUDRON_MAIL_SMTP_SERVER}
MailNotification\username=${CLOUDRON_MAIL_SMTP_USERNAME}
MailNotification\password=${CLOUDRON_MAIL_SMTP_PASSWORD}
EOF
fi

# migration from old auth, remove next release
sed -e '/AuthSubnetWhitelist/d' -i /app/data/qBittorrent/config/qBittorrent.conf

echo "==> update settings"
sed -e "s/Session\\\Port=.*/Session\\\Port=${QBITTORRENT_TCP_PORT}/" -i /app/data/qBittorrent/config/qBittorrent.conf

echo "==> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "==> Starting VueTorrent"
exec gosu cloudron:cloudron qbittorrent-nox --profile=/app/data
