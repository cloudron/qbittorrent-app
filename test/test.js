#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;

    let browser, app;
    const admin_username='admin', admin_password='adminadmin';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    });

    after(function () {
        browser.quit();
    });

    async function saveScreenshot(fileName) {
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${fileName.replaceAll(' ', '_')}-${new Date().getTime()}.png`, screenshotData, 'base64');
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');
        await saveScreenshot(this.currentTest.title);
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/`);

        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(., "Login")]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//button[@aria-label="Add torrent"] | //header//button[2]'));
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/#/');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//button[@aria-label="Add torrent"] | //header//button[2]'));
    }

    async function addTorrent() {
        await browser.get('https://' + app.fqdn + '/#/');
        await browser.findElement(By.xpath('//button[@aria-label="Add torrent"] | //header//button[2]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//label[text()="Select your files"] | //label[contains(., "Select files")]'));
        await browser.findElement(By.xpath('//input[@type="file" and @multiple]')).sendKeys(path.resolve(__dirname, 'sintel.torrent'));
        await browser.findElement(By.xpath('//button[contains(., "Add Torrent") or contains(., "Add torrents")]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//div[contains(text(), "Sintel")]'));
    }

    async function checkTorrent() {
        await browser.get('https://' + app.fqdn + '/#/');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//div[contains(text(), "Sintel")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('can add torrent', addTorrent);
    it('check torrent', checkTorrent);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('check torrent', checkTorrent);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('check torrent', checkTorrent);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('check torrent', checkTorrent);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id org.qbittorrent.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('can add torrent', addTorrent);
    it('check torrent', checkTorrent);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can login', login.bind(null, admin_username, admin_password));
    it('can get the main page', getMainPage);
    it('check torrent', checkTorrent);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

